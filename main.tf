provider "aws" {
    region = "ap-south-1"
    access_key = "AKIAZRMEB3HNDRXF57LF"
    secret_key = "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"
}

variable "subnet_cidr_block" {
    description = "subnet cidr block"
  }

resource "aws_vpc" "dev_vpc" {
    cidr_block = "10.0.0.0/16"
    tags = {
      Name: "Development"
    }
  }

resource "aws_subnet" "dev_subnet_1" {
    vpc_id = aws_vpc.dev_vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = "ap-south-1a"
    tags = {
        Name: "subnet_dev_1"
    }
  
}
data "aws_vpc" "existing_vpc" {
    default = true
}

resource "aws_subnet" "dev_subnet_2" {
    vpc_id = data.aws_vpc.existing_vpc.id
    cidr_block = "172.31.48.0/20"
    availability_zone = "ap-south-1a"
    tags = {
      Name: "subnet_dev_1_default"
    }
  
}

output "dev_vpc_id" {
    value =  aws_vpc.dev_vpc.id
  
}